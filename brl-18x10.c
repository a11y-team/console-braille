/*
 *  Copyright (C) Samuel Thibault <samuel.thibault@ens-lyon.org>
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation ; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program ; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#define DESSIN1 0,0,x,0,0,0,y,0,0,0,  0,0,0,0,0,0
#define DESSIN2 0,x,x,x,0,y,y,y,0,0,  0,0,0,0,0,0
#include <stdio.h>
#include "psf.h"
#include "do.h"
int main(void) {
  int i;
  header(10, 18, 256, 1);
  for (i=0;i<256;i++) {
    DO_SPC2();
    DO_DOTS1(1,4);
    DO_DOTS2(1,4);
    DO_DOTS1(1,4);
    DO_SPC2();
    DO_DOTS1(2,5);
    DO_DOTS2(2,5);
    DO_DOTS1(2,5);
    DO_SPC2();
    DO_DOTS1(3,6);
    DO_DOTS2(3,6);
    DO_DOTS1(3,6);
    DO_SPC2();
    DO_DOTS1(7,8);
    DO_DOTS2(7,8);
    DO_DOTS1(7,8);
    DO_SPC2();
    DO_SPC2();
  }
  for (i=0;i<256;i++)
    sfm(0x2800+i);
  return 0;
}
