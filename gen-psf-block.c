/*
 *  Copyright (C) Samuel Thibault <samuel.thibault@ens-lyon.org>
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation ; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program ; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include "psf.h"
int main(int argc, char *argv[]) {
  unsigned base;
  unsigned width, height, mask;
  char *end;

  if (argc != 4) {
    printf("Usage: %s [height] [width] xyz00\n", argv[0]);
    printf("where xyz00 is the hex value for the desired unicode block\n");
    exit(EXIT_FAILURE);
  }

  height = strtol(argv[1], &end, 10);
  if (end == argv[1] || *end != '\0') {
    fprintf(stderr,"Invalid height '%s'\n", argv[1]);
    exit(EXIT_FAILURE);
  }

  width = strtol(argv[2], &end, 10);
  if (end == argv[2] || *end != '\0') {
    fprintf(stderr,"Invalid width '%s'\n", argv[2]);
    exit(EXIT_FAILURE);
  }

  base = strtol(argv[3], &end, 16);
  if (end == argv[3] || *end != '\0') {
    fprintf(stderr,"Invalid block value '%s'\n", argv[3]);
    exit(EXIT_FAILURE);
  }

  int i, j;
  header(width, height, 256, 1);
  mask = ((1U << (width/2)) - 1) << (width/4);

  for (i=0;i<256;i++) {
    for (j=0;j<height;j++) {
      if (j >= height/4 && j < 3*height/4)
	if (width <= 8)
	  printf("%c", mask);
	else
	  printf("%c%c", mask >> 8, mask & 0xff);
      else
	if (width <= 8)
	  printf("%c", 0);
	else
	  printf("%c%c", 0, 0);
    }
  }

  for (i=0;i<256;i++)
    sfm(base+i);
  return 0;
}
