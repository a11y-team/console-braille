/*
 *  Copyright (C) Samuel Thibault <samuel.thibault@ens-lyon.org>
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation ; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program ; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <assert.h>

#ifndef TEST
/* Print byte from its binary encoding */
#define P(b7,b6,b5,b4,b3,b2,b1,b0) \
	printf("%c", b0*1+b1*2+b2*4+b3*8+b4*16+b5*32+b6*64+b7*128)
#define _P(a) P(a)

/* Print big-endian double byte from its binary encoding */
#define P2(b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	P(b15,b14,b13,b12,b11,b10,b9,b8); \
	P(b7,b6,b5,b4,b3,b2,b1,b0)
#define _P2(a) P2(a)

/* Print big-endian triple byte from its binary encoding */
#define P3(b23,b22,b21,b20,b19,b18,b17,b16,b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	P(b23,b22,b21,b20,b19,b18,b17,b16); \
	P(b15,b14,b13,b12,b11,b10,b9,b8); \
	P(b7,b6,b5,b4,b3,b2,b1,b0)
#define _P3(a) P3(a)

/* Print big-endian quadruple byte from its binary encoding */
#define P4(b31,b30,b29,b28,b27,b26,b25,b24,b23,b22,b21,b20,b19,b18,b17,b16,b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	P(b31,b30,b29,b28,b27,b26,b25,b24); \
	P(b23,b22,b21,b20,b19,b18,b17,b16); \
	P(b15,b14,b13,b12,b11,b10,b9,b8); \
	P(b7,b6,b5,b4,b3,b2,b1,b0)
#define _P4(a) P4(a)

/* Print big-endian quintuple byte from its binary encoding */
#define P5(b39,b38,b37,b36,b35,b34,b33,b32,b31,b30,b29,b28,b27,b26,b25,b24,b23,b22,b21,b20,b19,b18,b17,b16,b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	P(b39,b38,b37,b36,b35,b34,b33,b32); \
	P(b31,b30,b29,b28,b27,b26,b25,b24); \
	P(b23,b22,b21,b20,b19,b18,b17,b16); \
	P(b15,b14,b13,b12,b11,b10,b9,b8); \
	P(b7,b6,b5,b4,b3,b2,b1,b0)
#define _P5(a) P5(a)

/* Print big-endian sextuple byte from its binary encoding */
#define P6(b47,b46,b45,b44,b43,b42,b41,b40,b39,b38,b37,b36,b35,b34,b33,b32,b31,b30,b29,b28,b27,b26,b25,b24,b23,b22,b21,b20,b19,b18,b17,b16,b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	P(b47,b46,b45,b44,b43,b42,b41,b40); \
	P(b39,b38,b37,b36,b35,b34,b33,b32); \
	P(b31,b30,b29,b28,b27,b26,b25,b24); \
	P(b23,b22,b21,b20,b19,b18,b17,b16); \
	P(b15,b14,b13,b12,b11,b10,b9,b8); \
	P(b7,b6,b5,b4,b3,b2,b1,b0)
#define _P6(a) P6(a)

/* Print big-endian octuple byte from its binary encoding */
#define P8(b63,b62,b61,b60,b59,b58,b57,b56,b55,b54,b53,b52,b51,b50,b49,b48,b47,b46,b45,b44,b43,b42,b41,b40,b39,b38,b37,b36,b35,b34,b33,b32,b31,b30,b29,b28,b27,b26,b25,b24,b23,b22,b21,b20,b19,b18,b17,b16,b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	P(b63,b62,b61,b60,b59,b58,b57,b56); \
	P(b55,b54,b53,b52,b51,b50,b49,b48); \
	P(b47,b46,b45,b44,b43,b42,b41,b40); \
	P(b39,b38,b37,b36,b35,b34,b33,b32); \
	P(b31,b30,b29,b28,b27,b26,b25,b24); \
	P(b23,b22,b21,b20,b19,b18,b17,b16); \
	P(b15,b14,b13,b12,b11,b10,b9,b8); \
	P(b7,b6,b5,b4,b3,b2,b1,b0)
#define _P8(a) P8(a)


/* Print dots according to DESSIN */
#define _DO_DOTS(__x, __y) do { \
	int x = (__x), y = (__y);\
	_P(DESSIN); \
} while(0)
#define DO_DOTS(n,m) \
	_DO_DOTS( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1)))  \
			)

/* Print double-byte dots according to DESSIN1 */
#define _DO_DOTS1(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P2(DESSIN1); \
} while(0)
#define DO_DOTS1(n,m) \
	_DO_DOTS1( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print double-byte dots according to DESSIN2 */
#define _DO_DOTS2(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P2(DESSIN2); \
} while(0)
#define DO_DOTS2(n,m) \
	_DO_DOTS2( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)

/* Print triple-byte dots according to DESSIN1 */
#define _DO_DOTS31(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P3(DESSIN1); \
} while(0)
#define DO_DOTS31(n,m) \
	_DO_DOTS31( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print triple-byte dots according to DESSIN2 */
#define _DO_DOTS32(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P3(DESSIN2); \
} while(0)
#define DO_DOTS32(n,m) \
	_DO_DOTS32( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print triple-byte dots according to DESSIN3 */
#define _DO_DOTS33(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P3(DESSIN3); \
} while(0)
#define DO_DOTS33(n,m) \
	_DO_DOTS33( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)

/* Print quadruple-byte dots according to DESSIN1 */
#define _DO_DOTS41(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P4(DESSIN1); \
} while(0)
#define DO_DOTS41(n,m) \
	_DO_DOTS41( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print quadruple-byte dots according to DESSIN2 */
#define _DO_DOTS42(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P4(DESSIN2); \
} while(0)
#define DO_DOTS42(n,m) \
	_DO_DOTS42( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print quadruple-byte dots according to DESSIN3 */
#define _DO_DOTS43(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P4(DESSIN3); \
} while(0)
#define DO_DOTS43(n,m) \
	_DO_DOTS43( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)

/* Print quintuple-byte dots according to DESSIN1 */
#define _DO_DOTS51(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P5(DESSIN1); \
} while(0)
#define DO_DOTS51(n,m) \
	_DO_DOTS51( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print quintuple-byte dots according to DESSIN2 */
#define _DO_DOTS52(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P5(DESSIN2); \
} while(0)
#define DO_DOTS52(n,m) \
	_DO_DOTS52( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print quintuple-byte dots according to DESSIN3 */
#define _DO_DOTS53(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P5(DESSIN3); \
} while(0)
#define DO_DOTS53(n,m) \
	_DO_DOTS53( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print quintuple-byte dots according to DESSIN4 */
#define _DO_DOTS54(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P5(DESSIN4); \
} while(0)
#define DO_DOTS54(n,m) \
	_DO_DOTS54( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)

/* Print sextuple-byte dots according to DESSIN1 */
#define _DO_DOTS61(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P6(DESSIN1); \
} while(0)
#define DO_DOTS61(n,m) \
	_DO_DOTS61( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print sextuple-byte dots according to DESSIN2 */
#define _DO_DOTS62(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P6(DESSIN2); \
} while(0)
#define DO_DOTS62(n,m) \
	_DO_DOTS62( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print sextuple-byte dots according to DESSIN3 */
#define _DO_DOTS63(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P6(DESSIN3); \
} while(0)
#define DO_DOTS63(n,m) \
	_DO_DOTS63( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print sextuple-byte dots according to DESSIN4 */
#define _DO_DOTS64(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P6(DESSIN4); \
} while(0)
#define DO_DOTS64(n,m) \
	_DO_DOTS64( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)

/* Print octuple-byte dots according to DESSIN1 */
#define _DO_DOTS81(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P8(DESSIN1); \
} while(0)
#define DO_DOTS81(n,m) \
	_DO_DOTS81( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print octuple-byte dots according to DESSIN2 */
#define _DO_DOTS82(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P8(DESSIN2); \
} while(0)
#define DO_DOTS82(n,m) \
	_DO_DOTS82( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print octuple-byte dots according to DESSIN3 */
#define _DO_DOTS83(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P8(DESSIN3); \
} while(0)
#define DO_DOTS83(n,m) \
	_DO_DOTS83( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print octuple-byte dots according to DESSIN4 */
#define _DO_DOTS84(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P8(DESSIN4); \
} while(0)
#define DO_DOTS84(n,m) \
	_DO_DOTS84( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)
/* Print octuple-byte dots according to DESSIN5 */
#define _DO_DOTS85(__x,__y) do { \
	int x = (__x), y = (__y); \
	_P8(DESSIN5); \
} while(0)
#define DO_DOTS85(n,m) \
	_DO_DOTS85( \
		!!(i&(1<<(n-1))), \
		!!(i&(1<<(m-1))) \
		)


#else
#define HASH '#'
#define TOP \
	printf("/"); \
	for (j = 0; j < w; j++) \
		printf("-"); \
	printf("\\\n");
#define BOTTOM \
	printf("\\"); \
	for (j = 0; j < w; j++) \
		printf("-"); \
	printf("/\n");

#define LINEHEAD \
	if (i == 0) { TOP; continue; } \
	if (i == 1) n++; \
	if (i == 2) { assert(n == h); BOTTOM; break; } \

#define P(b7,b6,b5,b4,b3,b2,b1,b0) \
	LINEHEAD; \
	printf("|"); \
	if (w >= 1) printf("%c", b7 ? HASH : ' '); \
	if (w >= 2) printf("%c", b6 ? HASH : ' '); \
	if (w >= 3) printf("%c", b5 ? HASH : ' '); \
	if (w >= 4) printf("%c", b4 ? HASH : ' '); \
	if (w >= 5) printf("%c", b3 ? HASH : ' '); \
	if (w >= 6) printf("%c", b2 ? HASH : ' '); \
	if (w >= 7) printf("%c", b1 ? HASH : ' '); \
	if (w >= 8) printf("%c", b0 ? HASH : ' '); \
	printf("|\n");
#define _P(a) P(a)

#define P2(b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	LINEHEAD; \
	printf("|"); \
	if (w >= 1) printf("%c",b15 ? HASH : ' '); \
	if (w >= 2) printf("%c",b14 ? HASH : ' '); \
	if (w >= 3) printf("%c",b13 ? HASH : ' '); \
	if (w >= 4) printf("%c",b12 ? HASH : ' '); \
	if (w >= 5) printf("%c",b11 ? HASH : ' '); \
	if (w >= 6) printf("%c",b10 ? HASH : ' '); \
	if (w >= 7) printf("%c", b9 ? HASH : ' '); \
	if (w >= 8) printf("%c", b8 ? HASH : ' '); \
	if (w >= 9) printf("%c", b7 ? HASH : ' '); \
	if (w >= 10) printf("%c", b6 ? HASH : ' '); \
	if (w >= 11) printf("%c", b5 ? HASH : ' '); \
	if (w >= 12) printf("%c", b4 ? HASH : ' '); \
	if (w >= 13) printf("%c", b3 ? HASH : ' '); \
	if (w >= 14) printf("%c", b2 ? HASH : ' '); \
	if (w >= 15) printf("%c", b1 ? HASH : ' '); \
	if (w >= 16) printf("%c", b0 ? HASH : ' '); \
	printf("|\n");
#define _P2(a) P2(a)

#define P3(b23,b22,b21,b20,b19,b18,b17,b16,b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	LINEHEAD; \
	printf("|"); \
	if (w >= 1) printf("%c",b23 ? HASH : ' '); \
	if (w >= 2) printf("%c",b22 ? HASH : ' '); \
	if (w >= 3) printf("%c",b21 ? HASH : ' '); \
	if (w >= 4) printf("%c",b20 ? HASH : ' '); \
	if (w >= 5) printf("%c",b19 ? HASH : ' '); \
	if (w >= 6) printf("%c",b18 ? HASH : ' '); \
	if (w >= 7) printf("%c",b17 ? HASH : ' '); \
	if (w >= 8) printf("%c",b16 ? HASH : ' '); \
	if (w >= 9) printf("%c",b15 ? HASH : ' '); \
	if (w >= 10) printf("%c",b14 ? HASH : ' '); \
	if (w >= 11) printf("%c",b13 ? HASH : ' '); \
	if (w >= 12) printf("%c",b12 ? HASH : ' '); \
	if (w >= 13) printf("%c",b11 ? HASH : ' '); \
	if (w >= 14) printf("%c",b10 ? HASH : ' '); \
	if (w >= 15) printf("%c", b9 ? HASH : ' '); \
	if (w >= 16) printf("%c", b8 ? HASH : ' '); \
	if (w >= 17) printf("%c", b7 ? HASH : ' '); \
	if (w >= 18) printf("%c", b6 ? HASH : ' '); \
	if (w >= 19) printf("%c", b5 ? HASH : ' '); \
	if (w >= 20) printf("%c", b4 ? HASH : ' '); \
	if (w >= 21) printf("%c", b3 ? HASH : ' '); \
	if (w >= 22) printf("%c", b2 ? HASH : ' '); \
	if (w >= 23) printf("%c", b1 ? HASH : ' '); \
	if (w >= 24) printf("%c", b0 ? HASH : ' '); \
	printf("|\n");
#define _P3(a) P3(a)

#define P4(b31,b30,b29,b28,b27,b26,b25,b24,b23,b22,b21,b20,b19,b18,b17,b16,b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	LINEHEAD; \
	printf("|"); \
	if (w >= 1) printf("%c",b31 ? HASH : ' '); \
	if (w >= 2) printf("%c",b30 ? HASH : ' '); \
	if (w >= 3) printf("%c",b29 ? HASH : ' '); \
	if (w >= 4) printf("%c",b28 ? HASH : ' '); \
	if (w >= 5) printf("%c",b27 ? HASH : ' '); \
	if (w >= 6) printf("%c",b26 ? HASH : ' '); \
	if (w >= 7) printf("%c",b25 ? HASH : ' '); \
	if (w >= 8) printf("%c",b24 ? HASH : ' '); \
	if (w >= 9) printf("%c",b23 ? HASH : ' '); \
	if (w >= 10) printf("%c",b22 ? HASH : ' '); \
	if (w >= 11) printf("%c",b21 ? HASH : ' '); \
	if (w >= 12) printf("%c",b20 ? HASH : ' '); \
	if (w >= 13) printf("%c",b19 ? HASH : ' '); \
	if (w >= 14) printf("%c",b18 ? HASH : ' '); \
	if (w >= 15) printf("%c",b17 ? HASH : ' '); \
	if (w >= 16) printf("%c",b16 ? HASH : ' '); \
	if (w >= 17) printf("%c",b15 ? HASH : ' '); \
	if (w >= 18) printf("%c",b14 ? HASH : ' '); \
	if (w >= 19) printf("%c",b13 ? HASH : ' '); \
	if (w >= 20) printf("%c",b12 ? HASH : ' '); \
	if (w >= 21) printf("%c",b11 ? HASH : ' '); \
	if (w >= 22) printf("%c",b10 ? HASH : ' '); \
	if (w >= 23) printf("%c", b9 ? HASH : ' '); \
	if (w >= 24) printf("%c", b8 ? HASH : ' '); \
	if (w >= 25) printf("%c", b7 ? HASH : ' '); \
	if (w >= 26) printf("%c", b6 ? HASH : ' '); \
	if (w >= 27) printf("%c", b5 ? HASH : ' '); \
	if (w >= 28) printf("%c", b4 ? HASH : ' '); \
	if (w >= 29) printf("%c", b3 ? HASH : ' '); \
	if (w >= 30) printf("%c", b2 ? HASH : ' '); \
	if (w >= 31) printf("%c", b1 ? HASH : ' '); \
	if (w >= 32) printf("%c", b0 ? HASH : ' '); \
	printf("|\n");
#define _P4(a) P4(a)

#define P5(b39,b38,b37,b36,b35,b34,b33,b32,b31,b30,b29,b28,b27,b26,b25,b24,b23,b22,b21,b20,b19,b18,b17,b16,b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	LINEHEAD; \
	printf("|"); \
	if (w >= 1) printf("%c",b39 ? HASH : ' '); \
	if (w >= 2) printf("%c",b38 ? HASH : ' '); \
	if (w >= 3) printf("%c",b37 ? HASH : ' '); \
	if (w >= 4) printf("%c",b36 ? HASH : ' '); \
	if (w >= 5) printf("%c",b35 ? HASH : ' '); \
	if (w >= 6) printf("%c",b34 ? HASH : ' '); \
	if (w >= 7) printf("%c",b33 ? HASH : ' '); \
	if (w >= 8) printf("%c",b32 ? HASH : ' '); \
	if (w >= 9) printf("%c",b31 ? HASH : ' '); \
	if (w >= 10) printf("%c",b30 ? HASH : ' '); \
	if (w >= 11) printf("%c",b29 ? HASH : ' '); \
	if (w >= 12) printf("%c",b28 ? HASH : ' '); \
	if (w >= 13) printf("%c",b27 ? HASH : ' '); \
	if (w >= 14) printf("%c",b26 ? HASH : ' '); \
	if (w >= 15) printf("%c",b25 ? HASH : ' '); \
	if (w >= 16) printf("%c",b24 ? HASH : ' '); \
	if (w >= 17) printf("%c",b23 ? HASH : ' '); \
	if (w >= 18) printf("%c",b22 ? HASH : ' '); \
	if (w >= 19) printf("%c",b21 ? HASH : ' '); \
	if (w >= 20) printf("%c",b20 ? HASH : ' '); \
	if (w >= 21) printf("%c",b19 ? HASH : ' '); \
	if (w >= 22) printf("%c",b18 ? HASH : ' '); \
	if (w >= 23) printf("%c",b17 ? HASH : ' '); \
	if (w >= 24) printf("%c",b16 ? HASH : ' '); \
	if (w >= 25) printf("%c",b15 ? HASH : ' '); \
	if (w >= 26) printf("%c",b14 ? HASH : ' '); \
	if (w >= 27) printf("%c",b13 ? HASH : ' '); \
	if (w >= 28) printf("%c",b12 ? HASH : ' '); \
	if (w >= 29) printf("%c",b11 ? HASH : ' '); \
	if (w >= 30) printf("%c",b10 ? HASH : ' '); \
	if (w >= 31) printf("%c", b9 ? HASH : ' '); \
	if (w >= 32) printf("%c", b8 ? HASH : ' '); \
	if (w >= 33) printf("%c", b7 ? HASH : ' '); \
	if (w >= 34) printf("%c", b6 ? HASH : ' '); \
	if (w >= 35) printf("%c", b5 ? HASH : ' '); \
	if (w >= 36) printf("%c", b4 ? HASH : ' '); \
	if (w >= 37) printf("%c", b3 ? HASH : ' '); \
	if (w >= 38) printf("%c", b2 ? HASH : ' '); \
	if (w >= 39) printf("%c", b1 ? HASH : ' '); \
	if (w >= 40) printf("%c", b0 ? HASH : ' '); \
	printf("|\n");
#define _P5(a) P5(a)

#define P6(b47,b46,b45,b44,b43,b42,b41,b40,b39,b38,b37,b36,b35,b34,b33,b32,b31,b30,b29,b28,b27,b26,b25,b24,b23,b22,b21,b20,b19,b18,b17,b16,b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	LINEHEAD; \
	printf("|"); \
	if (w >= 1) printf("%c",b47 ? HASH : ' '); \
	if (w >= 2) printf("%c",b46 ? HASH : ' '); \
	if (w >= 3) printf("%c",b45 ? HASH : ' '); \
	if (w >= 4) printf("%c",b44 ? HASH : ' '); \
	if (w >= 5) printf("%c",b43 ? HASH : ' '); \
	if (w >= 6) printf("%c",b42 ? HASH : ' '); \
	if (w >= 7) printf("%c",b41 ? HASH : ' '); \
	if (w >= 8) printf("%c",b40 ? HASH : ' '); \
	if (w >= 9) printf("%c",b39 ? HASH : ' '); \
	if (w >= 10) printf("%c",b38 ? HASH : ' '); \
	if (w >= 11) printf("%c",b37 ? HASH : ' '); \
	if (w >= 12) printf("%c",b36 ? HASH : ' '); \
	if (w >= 13) printf("%c",b35 ? HASH : ' '); \
	if (w >= 14) printf("%c",b34 ? HASH : ' '); \
	if (w >= 15) printf("%c",b33 ? HASH : ' '); \
	if (w >= 16) printf("%c",b32 ? HASH : ' '); \
	if (w >= 17) printf("%c",b31 ? HASH : ' '); \
	if (w >= 18) printf("%c",b30 ? HASH : ' '); \
	if (w >= 19) printf("%c",b29 ? HASH : ' '); \
	if (w >= 20) printf("%c",b28 ? HASH : ' '); \
	if (w >= 21) printf("%c",b27 ? HASH : ' '); \
	if (w >= 22) printf("%c",b26 ? HASH : ' '); \
	if (w >= 23) printf("%c",b25 ? HASH : ' '); \
	if (w >= 24) printf("%c",b24 ? HASH : ' '); \
	if (w >= 25) printf("%c",b23 ? HASH : ' '); \
	if (w >= 26) printf("%c",b22 ? HASH : ' '); \
	if (w >= 27) printf("%c",b21 ? HASH : ' '); \
	if (w >= 28) printf("%c",b20 ? HASH : ' '); \
	if (w >= 29) printf("%c",b19 ? HASH : ' '); \
	if (w >= 30) printf("%c",b18 ? HASH : ' '); \
	if (w >= 31) printf("%c",b17 ? HASH : ' '); \
	if (w >= 32) printf("%c",b16 ? HASH : ' '); \
	if (w >= 33) printf("%c",b15 ? HASH : ' '); \
	if (w >= 34) printf("%c",b14 ? HASH : ' '); \
	if (w >= 35) printf("%c",b13 ? HASH : ' '); \
	if (w >= 36) printf("%c",b12 ? HASH : ' '); \
	if (w >= 37) printf("%c",b11 ? HASH : ' '); \
	if (w >= 38) printf("%c",b10 ? HASH : ' '); \
	if (w >= 39) printf("%c", b9 ? HASH : ' '); \
	if (w >= 40) printf("%c", b8 ? HASH : ' '); \
	if (w >= 41) printf("%c", b7 ? HASH : ' '); \
	if (w >= 42) printf("%c", b6 ? HASH : ' '); \
	if (w >= 43) printf("%c", b5 ? HASH : ' '); \
	if (w >= 44) printf("%c", b4 ? HASH : ' '); \
	if (w >= 45) printf("%c", b3 ? HASH : ' '); \
	if (w >= 46) printf("%c", b2 ? HASH : ' '); \
	if (w >= 47) printf("%c", b1 ? HASH : ' '); \
	if (w >= 48) printf("%c", b0 ? HASH : ' '); \
	printf("|\n");
#define _P6(a) P6(a)

#define P8(b63,b62,b61,b60,b59,b58,b57,b56,b55,b54,b53,b52,b51,b50,b49,b48,b47,b46,b45,b44,b43,b42,b41,b40,b39,b38,b37,b36,b35,b34,b33,b32,b31,b30,b29,b28,b27,b26,b25,b24,b23,b22,b21,b20,b19,b18,b17,b16,b15,b14,b13,b12,b11,b10,b9,b8,b7,b6,b5,b4,b3,b2,b1,b0) \
	LINEHEAD; \
	printf("|"); \
	if (w >= 1) printf("%c",b63 ? HASH : ' '); \
	if (w >= 2) printf("%c",b62 ? HASH : ' '); \
	if (w >= 3) printf("%c",b61 ? HASH : ' '); \
	if (w >= 4) printf("%c",b60 ? HASH : ' '); \
	if (w >= 5) printf("%c",b59 ? HASH : ' '); \
	if (w >= 6) printf("%c",b58 ? HASH : ' '); \
	if (w >= 7) printf("%c",b57 ? HASH : ' '); \
	if (w >= 8) printf("%c",b56 ? HASH : ' '); \
	if (w >= 9) printf("%c",b55 ? HASH : ' '); \
	if (w >= 10) printf("%c",b54 ? HASH : ' '); \
	if (w >= 11) printf("%c",b53 ? HASH : ' '); \
	if (w >= 12) printf("%c",b52 ? HASH : ' '); \
	if (w >= 13) printf("%c",b51 ? HASH : ' '); \
	if (w >= 14) printf("%c",b50 ? HASH : ' '); \
	if (w >= 15) printf("%c",b49 ? HASH : ' '); \
	if (w >= 16) printf("%c",b48 ? HASH : ' '); \
	if (w >= 17) printf("%c",b47 ? HASH : ' '); \
	if (w >= 18) printf("%c",b46 ? HASH : ' '); \
	if (w >= 19) printf("%c",b45 ? HASH : ' '); \
	if (w >= 20) printf("%c",b44 ? HASH : ' '); \
	if (w >= 21) printf("%c",b43 ? HASH : ' '); \
	if (w >= 22) printf("%c",b42 ? HASH : ' '); \
	if (w >= 23) printf("%c",b41 ? HASH : ' '); \
	if (w >= 24) printf("%c",b40 ? HASH : ' '); \
	if (w >= 25) printf("%c",b39 ? HASH : ' '); \
	if (w >= 26) printf("%c",b38 ? HASH : ' '); \
	if (w >= 27) printf("%c",b37 ? HASH : ' '); \
	if (w >= 28) printf("%c",b36 ? HASH : ' '); \
	if (w >= 29) printf("%c",b35 ? HASH : ' '); \
	if (w >= 30) printf("%c",b34 ? HASH : ' '); \
	if (w >= 31) printf("%c",b33 ? HASH : ' '); \
	if (w >= 32) printf("%c",b32 ? HASH : ' '); \
	if (w >= 33) printf("%c",b31 ? HASH : ' '); \
	if (w >= 34) printf("%c",b30 ? HASH : ' '); \
	if (w >= 35) printf("%c",b29 ? HASH : ' '); \
	if (w >= 36) printf("%c",b28 ? HASH : ' '); \
	if (w >= 37) printf("%c",b27 ? HASH : ' '); \
	if (w >= 38) printf("%c",b26 ? HASH : ' '); \
	if (w >= 39) printf("%c",b25 ? HASH : ' '); \
	if (w >= 40) printf("%c",b24 ? HASH : ' '); \
	if (w >= 41) printf("%c",b23 ? HASH : ' '); \
	if (w >= 42) printf("%c",b22 ? HASH : ' '); \
	if (w >= 43) printf("%c",b21 ? HASH : ' '); \
	if (w >= 44) printf("%c",b20 ? HASH : ' '); \
	if (w >= 45) printf("%c",b19 ? HASH : ' '); \
	if (w >= 46) printf("%c",b18 ? HASH : ' '); \
	if (w >= 47) printf("%c",b17 ? HASH : ' '); \
	if (w >= 48) printf("%c",b16 ? HASH : ' '); \
	if (w >= 49) printf("%c",b15 ? HASH : ' '); \
	if (w >= 50) printf("%c",b14 ? HASH : ' '); \
	if (w >= 51) printf("%c",b13 ? HASH : ' '); \
	if (w >= 52) printf("%c",b12 ? HASH : ' '); \
	if (w >= 53) printf("%c",b11 ? HASH : ' '); \
	if (w >= 54) printf("%c",b10 ? HASH : ' '); \
	if (w >= 55) printf("%c", b9 ? HASH : ' '); \
	if (w >= 56) printf("%c", b8 ? HASH : ' '); \
	if (w >= 57) printf("%c", b7 ? HASH : ' '); \
	if (w >= 58) printf("%c", b6 ? HASH : ' '); \
	if (w >= 59) printf("%c", b5 ? HASH : ' '); \
	if (w >= 60) printf("%c", b4 ? HASH : ' '); \
	if (w >= 61) printf("%c", b3 ? HASH : ' '); \
	if (w >= 62) printf("%c", b2 ? HASH : ' '); \
	if (w >= 63) printf("%c", b1 ? HASH : ' '); \
	if (w >= 64) printf("%c", b0 ? HASH : ' '); \
	printf("|\n");
#define _P8(a) P8(a)

#define DO_DOTS(__x,__y) \
	_P(DESSIN);

#define DO_DOTS1(__x,__y) \
	_P2(DESSIN1);
#define DO_DOTS2(__x,__y) \
	_P2(DESSIN2);

#define DO_DOTS31(__x,__y) \
	_P3(DESSIN1);
#define DO_DOTS32(__x,__y) \
	_P3(DESSIN2);
#define DO_DOTS33(__x,__y) \
	_P3(DESSIN3);

#define DO_DOTS41(__x,__y) \
	_P4(DESSIN1);
#define DO_DOTS42(__x,__y) \
	_P4(DESSIN2);
#define DO_DOTS43(__x,__y) \
	_P4(DESSIN3);

#define DO_DOTS51(__x,__y) \
	_P5(DESSIN1);
#define DO_DOTS52(__x,__y) \
	_P5(DESSIN2);
#define DO_DOTS53(__x,__y) \
	_P5(DESSIN3);
#define DO_DOTS54(__x,__y) \
	_P5(DESSIN4);

#define DO_DOTS61(__x,__y) \
	_P6(DESSIN1);
#define DO_DOTS62(__x,__y) \
	_P6(DESSIN2);
#define DO_DOTS63(__x,__y) \
	_P6(DESSIN3);
#define DO_DOTS64(__x,__y) \
	_P6(DESSIN4);

#define DO_DOTS81(__x,__y) \
	_P8(DESSIN1);
#define DO_DOTS82(__x,__y) \
	_P8(DESSIN2);
#define DO_DOTS83(__x,__y) \
	_P8(DESSIN3);
#define DO_DOTS84(__x,__y) \
	_P8(DESSIN4);
#define DO_DOTS85(__x,__y) \
	_P8(DESSIN5);

#endif

/* Print space */
#define DO_SPC() \
	P(0,0,0,0,0,0,0,0)
/* Print double-byte space */
#define DO_SPC2() \
	P2(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
/* Print triple-byte space */
#define DO_SPC3() \
	P3(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
/* Print quadruple-byte space */
#define DO_SPC4() \
	P4(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
/* Print quintuple-byte space */
#define DO_SPC5() \
	P5(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
/* Print sextuple-byte space */
#define DO_SPC6() \
	P6(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
/* Print octuple-byte space */
#define DO_SPC8() \
	P8(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

