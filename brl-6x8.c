/*
 *  Copyright (C) Samuel Thibault <samuel.thibault@ens-lyon.org>
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation ; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program ; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#define DESSIN 0,x,x,0,y,y,0,0
#include <stdio.h>
#include "psf.h"
#include "do.h"
int main(void) {
  int i;
  header(8, 6, 256, 1);
  for (i=0;i<256;i++) {
    DO_SPC();
    DO_DOTS(1,4);
    DO_DOTS(2,5);
    DO_DOTS(3,6);
    DO_DOTS(7,8);
    DO_SPC();
  }
  for (i=0;i<256;i++)
    sfm(0x2800+i);
  return 0;
}
