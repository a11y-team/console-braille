BRLS:=$(wildcard brl-*.c)
TARGETS:=$(BRLS:.c=.psf)
PROGS:=$(BRLS:.c=)
TESTPROGS:=$(BRLS:.c=.test)
TESTS:=$(BRLS:.c=.txt)

BUILD_CC ?= $(CC)

UNAME=$(shell uname)

all: $(TARGETS) brl.uni gen-psf-block

test: $(TESTS)

ifeq ($(UNAME),Linux)
all: setbrlkeys
endif


brl-%: brl-%.build.o psf.build.o
	$(BUILD_CC) -o $@ $(LDFLAGS) $^ $(LDLIBS)

gen-psf-block: gen-psf-block.o psf.o

%.build: %.c
	$(BUILD_CC) $(CFLAGS) $(CPPFLAGS) -o $@ $(LDFLAGS) $^ $(LDLIBS)


%.build.o: %.c
	$(BUILD_CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<

%.test: %.c do.h
	$(BUILD_CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -DTEST -o $@ $<


.INTERMEDIATE: $(PROGS) $(TESTPROGS) uni.build

$(TARGETS): %.psf: %
	./$< > $@

$(TESTS): %.txt: %.test
	./$< > $@

brl.uni: uni.build
	./$< 0 > $@


clean:
	rm -f $(PROGS) $(TESTPROGS) psf.o psf.build.o $(BRLS:.c=.o) gen-psf-block.o $(TARGETS) $(TESTS) brl.uni uni.build setbrlkeys gen-psf-block
