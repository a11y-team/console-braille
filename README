Braille fonts and keymaps for the linux console
===============================================

This package contains support for two things:

- being able to type unicode braille patterns directly on the text console.

  This permits to type raw braille patterns: they will show up as braille dots
  on the screen, and if you send them within files or mails to people, they will
  read exactly the same pattern as you typed.  No interpretation ("this is an
  'A'" for instance) will be done.

- being able to read any non-ascii script with screen readers such as brltty.

  One actually just need to load the corresponding PSF font for the system to be
  able to display them, and thus for brltty and such to be able to read them.
  There are however not so many PSF fonts, and certainly not for all unicode
  blocks. But if the only goal is to be able to read the text with brltty,
  without caring about what actually shows up on the screen, we can easily
  generate a dumb font which will just display dumb letters, while brltty will
  be able to read the text.

But for all of this, unicode environment, fonts and keymaps are needed.

Unicode environment
-------------------

Your environment needs to be configured in Unicode mode (aka UTF-8).  Recent
distributions are configured this way.  If you don't know what this means, check
that

locale charmap

returns UTF-8.  If it does not, please read documentation from your distribution
for configuring a unicode environment.  Note: this will change the way accented
letters etc. will work.

On Debian, run

dpkg-reconfigure locales

and select a UTF-8 version of your locale, then enable it by default.  Reboot to
get things configured.

Fonts
-----

Running

  setfont lat9-16 brl-16x8

This will load 16-pixel height latin9 and braille pattern fonts into the text
console.

If you want to read e.g. Devanagari text, according to Unicode this is block
U+0900, thus run

  gen-psf-block-16x8 0900 > devanagari-16x8.psf

and then

  setfont lat9-16 devanagari-16x8

will allow to read both Latin-9 and Devanagari texts with e.g brltty.

Keymaps
-------

Keymaps for typing braille various machine types are given in keymaps/.  They
will hopefully be included in distributions.  With them, you need to hold left
and right alt keys (AKA alt and altgr keys) pressed, and then use asdfjkl; keys
(or qsdfjkml keys on an azerty keyboard, etc.) for typing braille patterns just
like you would use a usual braille device.

But for more advanced keymaps, run

  make

That will create the setbrlkeys program, which sets braille keymaps for PC
keyboards: it sets the "pause" key (the one around the topright corner of the
keyboard) as a "braille lock" key, to activate the following braille keymaps:

- [qa]sdfjkl[m;] are bound to dots 73214568
- 01456+ are bound to dots 321456
- ghvn are bound to the empty pattern

If you give the -l parameter to setbrlkeys, it will bind [qa]sdfbv to 321456.
If you give the -r parameter to setbrlkeys, it will bind jbnkl[m;] to 123456.

Unfortunately, some bad keyboards (like laptop ones) don't like several keys to
be pressed at once.  In such case, you can try to run

  echo -1 > /sys/module/keyboard/parameters/brl_timeout

This will make your keys produce "dead" dot patterns: just type your dot pattern
one dot after the other, and finish the braille pattern by typing space.

FAQ
---

Q- When I press pattern, nothing is display, but "keyboard mode must be unicode
for braille patter" is output in Linux's dmesg.
A- Your distribution forgot to notify the kernel that it is using unicode.  For
loading the font, instead of using setfont, use:

unicode_start lat9wbrl-16x8.psf lat9wbrl.uni

Q- The patterns are incomplete, some dots are missing
A- Maybe your keyboard is just bad.  Laptop for instance often have keyboards
whose keys are not very independant, and thus Linux just can not detect all
presses.  There is nothing which can be done but choose a different layout, or
buy a better keyboard...
