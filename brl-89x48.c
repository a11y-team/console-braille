/*
 *  Copyright (C) Samuel Thibault <samuel.thibault@ens-lyon.org>
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation ; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program ; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#define DESSIN1 0,0,0,0,0,0,0,0,x,x,x,x,x,x,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,y,y,y,y,y,y,0,0,0,0,0,0,0,0,0,0,0,0
#define DESSIN2 0,0,0,0,0,0,x,x,x,x,x,x,x,x,x,x,0,0,0,0,0,0,0,0,0,0,0,0,y,y,y,y,y,y,y,y,y,y,0,0,0,0,0,0,0,0,0,0
#define DESSIN3 0,0,0,0,0,x,x,x,x,x,x,x,x,x,x,x,x,0,0,0,0,0,0,0,0,0,0,y,y,y,y,y,y,y,y,y,y,y,y,0,0,0,0,0,0,0,0,0
#define DESSIN4 0,0,0,0,x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,0,0,0,0,0,0,0,y,y,y,y,y,y,y,y,y,y,y,y,y,y,0,0,0,0,0,0,0,0
#include <stdio.h>
#include "psf.h"
#include "do.h"
int main(void) {
  int i;
  header(48, 89, 256, 1);
  for (i=0;i<256;i++) {
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_DOTS61(1,4);
    DO_DOTS62(1,4);
    DO_DOTS63(1,4);
    DO_DOTS63(1,4);
    DO_DOTS64(1,4);
    DO_DOTS64(1,4);
    DO_DOTS64(1,4);
    DO_DOTS64(1,4);
    DO_DOTS64(1,4);
    DO_DOTS63(1,4);
    DO_DOTS63(1,4);
    DO_DOTS62(1,4);
    DO_DOTS61(1,4);
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_DOTS61(2,5);
    DO_DOTS62(2,5);
    DO_DOTS63(2,5);
    DO_DOTS63(2,5);
    DO_DOTS64(2,5);
    DO_DOTS64(2,5);
    DO_DOTS64(2,5);
    DO_DOTS64(2,5);
    DO_DOTS64(2,5);
    DO_DOTS63(2,5);
    DO_DOTS63(2,5);
    DO_DOTS62(2,5);
    DO_DOTS61(2,5);
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_DOTS61(3,6);
    DO_DOTS62(3,6);
    DO_DOTS63(3,6);
    DO_DOTS63(3,6);
    DO_DOTS64(3,6);
    DO_DOTS64(3,6);
    DO_DOTS64(3,6);
    DO_DOTS64(3,6);
    DO_DOTS64(3,6);
    DO_DOTS63(3,6);
    DO_DOTS63(3,6);
    DO_DOTS62(3,6);
    DO_DOTS61(3,6);
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_DOTS61(7,8);
    DO_DOTS62(7,8);
    DO_DOTS63(7,8);
    DO_DOTS63(7,8);
    DO_DOTS64(7,8);
    DO_DOTS64(7,8);
    DO_DOTS64(7,8);
    DO_DOTS64(7,8);
    DO_DOTS64(7,8);
    DO_DOTS63(7,8);
    DO_DOTS63(7,8);
    DO_DOTS62(7,8);
    DO_DOTS61(7,8);
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
    DO_SPC6();
  }
  for (i=0;i<256;i++)
    sfm(0x2800+i);
  return 0;
}
