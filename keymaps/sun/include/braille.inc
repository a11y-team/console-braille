# 
#  Copyright (C) Samuel Thibault <samuel.thibault@ens-lyon.org>
# 
# This program is free software ; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation ; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY ; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with the program ; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# 

alt altgr keycode 0x4d = Brl_dot7
alt altgr keycode 0x4e = Brl_dot3
alt altgr keycode 0x4f = Brl_dot2
alt altgr keycode 0x50 = Brl_dot1
alt altgr keycode 0x53 = Brl_dot4
alt altgr keycode 0x54 = Brl_dot5
alt altgr keycode 0x55 = Brl_dot6
alt altgr keycode 0x56 = Brl_dot8
