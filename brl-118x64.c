/*
 *  Copyright (C) Samuel Thibault <samuel.thibault@ens-lyon.org>
 *
 * This program is free software ; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation ; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program ; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#define DESSIN1 0,0,0,0,0,0,0,0,0,0,0,0,0,x,x,x,x,x,x,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,y,y,y,y,y,y,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
#define DESSIN2 0,0,0,0,0,0,0,0,0,0,0,x,x,x,x,x,x,x,x,x,x,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,y,y,y,y,y,y,y,y,y,y,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
#define DESSIN3 0,0,0,0,0,0,0,0,0,x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,0,0,0,0,0,0,0,0,0,0,0,0,0,y,y,y,y,y,y,y,y,y,y,y,y,y,y,0,0,0,0,0,0,0,0,0,0,0,0,0
#define DESSIN4 0,0,0,0,0,0,0,0,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,0,0,0,0,0,0,0,0,0,0,0,y,y,y,y,y,y,y,y,y,y,y,y,y,y,y,y,0,0,0,0,0,0,0,0,0,0,0,0
#define DESSIN5 0,0,0,0,0,0,0,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,0,0,0,0,0,0,0,0,0,y,y,y,y,y,y,y,y,y,y,y,y,y,y,y,y,y,y,0,0,0,0,0,0,0,0,0,0,0
#include <stdio.h>
#include "psf.h"
#include "do.h"
int main(void) {
  int i;
  header(64, 118, 256, 1);
  for (i=0;i<256;i++) {
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_DOTS81(1,4);
    DO_DOTS82(1,4);
    DO_DOTS83(1,4);
    DO_DOTS83(1,4);
    DO_DOTS84(1,4);
    DO_DOTS84(1,4);
    DO_DOTS85(1,4);
    DO_DOTS85(1,4);
    DO_DOTS85(1,4);
    DO_DOTS85(1,4);
    DO_DOTS85(1,4);
    DO_DOTS84(1,4);
    DO_DOTS84(1,4);
    DO_DOTS83(1,4);
    DO_DOTS83(1,4);
    DO_DOTS82(1,4);
    DO_DOTS81(1,4);
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_DOTS81(2,5);
    DO_DOTS82(2,5);
    DO_DOTS83(2,5);
    DO_DOTS83(2,5);
    DO_DOTS84(2,5);
    DO_DOTS84(2,5);
    DO_DOTS85(2,5);
    DO_DOTS85(2,5);
    DO_DOTS85(2,5);
    DO_DOTS85(2,5);
    DO_DOTS85(2,5);
    DO_DOTS84(2,5);
    DO_DOTS84(2,5);
    DO_DOTS83(2,5);
    DO_DOTS83(2,5);
    DO_DOTS82(2,5);
    DO_DOTS81(2,5);
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_DOTS81(3,6);
    DO_DOTS82(3,6);
    DO_DOTS83(3,6);
    DO_DOTS83(3,6);
    DO_DOTS84(3,6);
    DO_DOTS84(3,6);
    DO_DOTS85(3,6);
    DO_DOTS85(3,6);
    DO_DOTS85(3,6);
    DO_DOTS85(3,6);
    DO_DOTS85(3,6);
    DO_DOTS84(3,6);
    DO_DOTS84(3,6);
    DO_DOTS83(3,6);
    DO_DOTS83(3,6);
    DO_DOTS82(3,6);
    DO_DOTS81(3,6);
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_DOTS81(7,8);
    DO_DOTS82(7,8);
    DO_DOTS83(7,8);
    DO_DOTS83(7,8);
    DO_DOTS84(7,8);
    DO_DOTS84(7,8);
    DO_DOTS85(7,8);
    DO_DOTS85(7,8);
    DO_DOTS85(7,8);
    DO_DOTS85(7,8);
    DO_DOTS85(7,8);
    DO_DOTS84(7,8);
    DO_DOTS84(7,8);
    DO_DOTS83(7,8);
    DO_DOTS83(7,8);
    DO_DOTS82(7,8);
    DO_DOTS81(7,8);
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
    DO_SPC8();
  }
  for (i=0;i<256;i++)
    sfm(0x2800+i);
  return 0;
}
